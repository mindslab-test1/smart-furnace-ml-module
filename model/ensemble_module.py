import torch
import torch.nn as nn
import torch.nn.functional as F

class EnsembleModule(nn.Module):
    def __init__(self, num_models):
        super(type(self), self).__init__()
        self.fc = nn.Conv1d(num_models, 1, 1)

    def forward(self, x):
        return self.fc(x)