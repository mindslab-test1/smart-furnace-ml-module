import torch
import torch.nn as nn
import torch.nn.functional as F

import numpy as np

class Net(nn.Module):
    def __init__(self, hp):
        super(type(self), self).__init__()
        self.hp = hp
        self.feature_extractor = nn.Sequential(
            nn.Dropout(0.5),
            nn.Linear(hp.data.num_dataset_classes, 64),
            nn.ReLU(),
            nn.Dropout(0.5),
            nn.Linear(64, 64),
            nn.ReLU(),
            nn.Dropout(0.5),
            nn.Linear(64, 64),
            nn.ReLU(),
            nn.Dropout(0.5),
            nn.Linear(64, 64),
            nn.ReLU(),
            nn.Dropout(0.5)
        )
        self.lstm = nn.LSTM(
            input_size=64,
            hidden_size=64,
            batch_first=True,
            num_layers=1,
            dropout=0.5
        )
        self.class_fc = nn.Sequential(
            nn.Linear(64, 4),
            # output of this whole layer: [K-T, K-U, K-M, K-L]
        )

    def forward(self, x):  # x: [B, hp.model.LSTM.receptive_field, hp.data.num_dataset_classes]
        x = self.feature_extractor(x)  # x: [B, hp.model.LSTM.receptive_field, 128]
        x = self.lstm(x)[0]  # [B, hp.model.LSTM.receptive_field, 128]
        x = x[:, -1, :]  # [B, 1, hp.data.num_dataset_classes]
        x = self.class_fc(x)  # [B, 1, 4]
        x = x.squeeze(1)  # [B, 4]
        return nn.Sigmoid()(x)

    def inference(self, x):
        return self.forward(x)

    def get_loss(self, output, target):
        loss = nn.MSELoss()
        return loss(output, target)
