import torch
import torch.nn as nn
import torch.nn.functional as F
from model.model import Net
from model.ensemble_module import EnsembleModule


class ModelList():
    def __init__(self, hp, num_models):
        self.models = [Net(hp).cuda() for _ in range(num_models)]
        self.fc = EnsembleModule(num_models).cuda()
        self.score_of_models = torch.zeros(num_models).cuda()
        self.num_models = num_models
        self.hp = hp

    def get_rank_list(self, input_list):
        # return list of rank of input list element
        # e.g.)
        # input_list = [4, 7, 9, 10, 6, 11, 3]
        # return_list = [1, 3, 4, 5, 2, 6, 0]
        indices = list(range(len(input_list)))
        indices.sort(key=lambda x: input_list[x])
        output = [0] * len(indices)
        for i, x in enumerate(indices):
            output[x] = i
        return output

    def forward(self, x):  # x: [B, hp.model.LSTM.seq_len, hp.data.num_dataset_classes]
        x = [model(x) for model in self.models]  # [num_models, batch, Net_output]
        return x

    def inference(self, x):  # x: [B, hp.model.LSTM.seq_len, hp.data.num_dataset_classes]
        x = self.forward(x)  # [num_models, batch, Net_output]
        x = torch.stack(x)
        x = torch.transpose(x, 0, 1)  # [batch, num_models, Net_output]
        x = self.fc(x)  # [batch, 1, Net_output]
        x = x.squeeze(1)  # [batch, Net_output]
        return x

    def amnl_loss(self, outputs, target, is_validation=False):
        loss_func = nn.MSELoss()
        model_losses = list()
        for output in outputs:
            model_losses.append(loss_func(output, target))
        loss_list = model_losses  # [num_models]
        if is_validation:
            # model_ranks = torch.tensor(self.get_rank_list(model_losses))
            self.score_of_models += torch.stack(loss_list).detach()
        return loss_list

    def next_generation(self, num_remove_models):
        def weights_init(m):
            for layer in m.children():
                if hasattr(layer, 'reset_parameters'):
                    layer.reset_parameters()
        rank_of_scores = self.get_rank_list(self.score_of_models)
        for i, rank in enumerate(rank_of_scores):
            if rank >= self.num_models - num_remove_models:
                weights_init(self.models[i])
        self.score_of_models = torch.zeros(self.num_models).cuda()

    def fc_loss(self, output, target):
        loss_func = nn.MSELoss()
        return loss_func(output, target)
