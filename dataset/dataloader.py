import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
import glob
import os
import csv


def create_dataloader(hp, args, train, validation):
    if train:
        return DataLoader(dataset=Dataset_(hp, args, True, False),

                          batch_size=hp.train.batch_size,
                          shuffle=True,
                          num_workers=hp.train.num_workers,
                          pin_memory=True,
                          drop_last=True)
    else:
        if validation:
            return DataLoader(dataset=Dataset_(hp, args, False, True),
                              batch_size=hp.validation.batch_size,
                              shuffle=False,
                              num_workers=hp.validation.num_workers,
                              pin_memory=True,
                              drop_last=True)
        else:
            return DataLoader(dataset=Dataset_(hp, args, False, False),
                              batch_size=hp.test.batch_size,
                              shuffle=False,
                              num_workers=hp.test.num_workers,
                              pin_memory=True,
                              drop_last=True)


class Dataset_(Dataset):
    def __init__(self, hp, args, train, validation):
        self.hp = hp
        self.args = args
        self.train = train
        if train:
            self.data_dir = hp.data.train
        elif validation:
            self.data_dir = hp.data.validation
        else:
            self.data_dir = hp.data.test
        self.dataset_files = sorted(glob.glob(os.path.join(self.data_dir, self.hp.data.file_format)))
        self.dataset = list()

        def normalize(data):
            data_min = torch.min(data, dim=0)[0].repeat(data.shape[0], 1)
            data_max = torch.max(data, dim=0)[0].repeat(data.shape[0], 1)
            margin = data_max - data_min
            margin = torch.where(margin == 0, torch.ones_like(margin), margin)
            data = (data - data_min) / margin
            return data

        for dataset_file in self.dataset_files:
            with open(os.path.join(self.data_dir, dataset_file), 'r',  newline='') as csvfile:
                csv_readlines = list(csv.DictReader(csvfile))
            for x in csv_readlines:  # remove index from data
                x.pop(hp.data.time_var_name, None)
            self.inputs = [list(map(float, list(x.values()))) for x in csv_readlines]
            self.targets = [list(map(float, [
                x['Total_K'],
                x['Upper_K'],
                x['Middle_K'],
                x['Lower_K']
            ])) for x in csv_readlines]
            self.inputs = torch.tensor(self.inputs)
            self.targets = torch.tensor(self.targets)

            self.inputs = normalize(self.inputs)
            self.targets = normalize(self.targets)
        
            seq_len = hp.model.LSTM.seq_len
            self.dataset = self.dataset + [
                (self.inputs[i - seq_len:i], self.targets[i + hp.model.predict_after - 1])
                for i in range(self.inputs.shape[0])
                if seq_len <= i < self.inputs.shape[0] - hp.model.predict_after
            ]
            # [i - seq_len, i): using value to predict future
            #                           (i - 1 is present and i is future after 1 minute)
            # i + predict_after - 1: future value to be predicted
            # dataset: list of tuple(using value to predict future, future value)
            #          == list of tuple(input value list, target value)

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, idx):
        return self.dataset[idx]
