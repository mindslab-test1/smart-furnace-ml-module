import os
import math
import tqdm
import torch
import itertools
import traceback

from .test_model import testing_model
from .utils import get_commit_hash
from model.model import Net
from model.AMNL_model_list import ModelList


def validate(model, validate_loader, writer, step, num_models, batch_size):
    for m in model.models:
        m.eval()
    model.fc.eval()
    loader = tqdm.tqdm(validate_loader, desc='Validation data loader')
    total_validation_loss = torch.zeros(num_models).cuda()
    model.score_of_models = torch.zeros(num_models).cuda()
    for model_input, target in loader:
        model_input = model_input.cuda()
        target = target.cuda()
        outputs = model.forward(model_input)
        loss = model.amnl_loss(outputs, target, is_validation=True)
        total_validation_loss += torch.stack(loss).detach()
    total_validation_loss /= (len(validate_loader.dataset) // batch_size)  # divide with number of batches
    writer.validation_logging(total_validation_loss, step)


def train(args, pt_dir, chkpt_path, train_loader, validation_loader, test_loader, writer, logger, hp, hp_str):
    git_hash = get_commit_hash()
    generation_iter_idx = -1
    num_generation = hp.train.AMNL.num_generation
    if chkpt_path is not None:
        logger.info("Resuming from checkpoint: %s" % chkpt_path)
        checkpoint = torch.load(chkpt_path)
        generation_iter_idx = checkpoint['generation_iter_idx']
    else:
        logger.info("Starting new training run.")

    num_models = hp.model.AMNL.num_models
    amnl_models = ModelList(hp, num_models)
    if hp.train.optimizer == 'adam':
        amnl_optimizers = [torch.optim.Adam(
            amnl_models.models[i].parameters(), lr=hp.train.adam.initlr) for i in range(num_models)]
        fc_optimizer = torch.optim.Adam(amnl_models.fc.parameters(), lr=hp.train.adam.initlr)
    else:
        raise Exception("%s optimizer not supported" % hp.train.optimizer)

    step = 0
    init_epoch = -1
    # loading checkpoint
    if chkpt_path is not None:
        logger.info("Resuming from checkpoint: %s" % chkpt_path)
        checkpoint = torch.load(chkpt_path)
        for i, m in enumerate(amnl_models.models):
            m.load_state_dict(checkpoint['amnl_models'][i])
        amnl_models.fc.load_state_dict(checkpoint['fc_model'])
        for i, op in enumerate(amnl_optimizers):
            op.load_state_dict(checkpoint['amnl_optimizers'][i])
        fc_optimizer.load_state_dict(checkpoint['fc_optimizer'])
        step = checkpoint['step']
        init_epoch = checkpoint['epoch']
        git_hash = checkpoint['git_hash']
        if hp_str != checkpoint['hp_str']:
            logger.warning("New hparams is different from checkpoint. Will use new.")

    if num_generation > generation_iter_idx + 1:
        # Asynchronous Multi Network Learning / train Net module
        for generation_idx in range(generation_iter_idx + 1, num_generation):
            # train
            epoch_size = hp.train.AMNL.epoch_size
            for epoch in range(epoch_size):
                for model in amnl_models.models:
                    model.train()
                amnl_models.fc.train()
                loader = tqdm.tqdm(train_loader, desc='Train data loader')
                for model_input, target in loader:
                    model_input = model_input.cuda()
                    target = target.cuda()
                    outputs = amnl_models.forward(model_input)
                    losses = amnl_models.amnl_loss(outputs, target)
                    for i, loss in enumerate(losses):
                        amnl_optimizers[i].zero_grad()
                        loss.backward()
                        amnl_optimizers[i].step()
                    step += 1
                    losses_stack = torch.stack(losses)
                    loss_item = losses_stack.mean().item()
                    if loss_item > 1e8 or math.isnan(loss_item):
                        logger.error("Loss exploded to %.02f at step %d (generation_idx: %d)!" % (loss_item, step, generation_idx))
                        raise Exception("Loss exploded")
                    if step % hp.log.summary_interval == 0:
                        writer.amnl_train_logging(losses_stack, step)
                        loader.set_description('Loss %.02f at step %d (generation_idx: %d)' % (loss_item, step, generation_idx))

                generation_iter_idx = generation_idx
                save_path = os.path.join(pt_dir, '%s_%s_g%03d_%03d.pt' % (args.name, git_hash, generation_iter_idx, epoch))
                torch.save({
                    'amnl_models': [model.state_dict() for model in amnl_models.models],
                    'fc_model': amnl_models.fc.state_dict(),
                    'amnl_optimizers': [optimizer.state_dict() for optimizer in amnl_optimizers],
                    'fc_optimizer': fc_optimizer.state_dict(),
                    'step': step,
                    'epoch': epoch,
                    'hp_str': hp_str,
                    'git_hash': git_hash,
                    'generation_iter_idx': generation_iter_idx,
                }, save_path)
                logger.info("Saved checkpoint to: %s" % save_path)

                validate(amnl_models, validation_loader, writer, step, num_models, batch_size=hp.validation.batch_size)

            amnl_models.next_generation(hp.model.AMNL.num_remove_models)

    # train fc(ensemble) network
    try:
        for epoch in itertools.count(init_epoch+1):
            for model in amnl_models.models:
                model.train()
            amnl_models.fc.train()
            loader = tqdm.tqdm(train_loader, desc='Train data loader')
            for model_input, target in loader:
                model_input = model_input.cuda()
                target = target.cuda()
                output = amnl_models.inference(model_input)
                loss = amnl_models.fc_loss(output, target)

                fc_optimizer.zero_grad()
                loss.backward()
                fc_optimizer.step()
                step += 1

                loss = loss.item()
                if loss > 1e8 or math.isnan(loss):
                    logger.error("Loss exploded to %.02f at step %d!" % (loss, step))
                    raise Exception("Loss exploded")

                if step % hp.log.summary_interval == 0:
                    writer.fc_train_logging(loss, step)
                    loader.set_description('Loss %.02f at step %d' % (loss, step))

            save_path = os.path.join(pt_dir, '%s_%s_g%03d_%03d.pt' % (args.name, git_hash, generation_iter_idx, epoch))
            torch.save({
                'amnl_models': [model.state_dict() for model in amnl_models.models],
                'fc_model': amnl_models.fc.state_dict(),
                'amnl_optimizers': [optimizer.state_dict() for optimizer in amnl_optimizers],
                'fc_optimizer': fc_optimizer.state_dict(),
                'step': step,
                'epoch': epoch,
                'hp_str': hp_str,
                'git_hash': git_hash,
                'generation_iter_idx': generation_iter_idx,
            }, save_path)
            logger.info("Saved checkpoint to: %s" % save_path)

            testing_model(amnl_models, test_loader, writer, step, hp)

    except Exception as e:
        logger.info("Exiting due to exception: %s" % e)
        traceback.print_exc()
