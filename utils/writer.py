import numpy as np
from tensorboardX import SummaryWriter
from . import plotting as plt


class Writer(SummaryWriter):
    def __init__(self, hp, logdir):
        super(type(self), self).__init__(logdir)
        self.hp = hp

    def amnl_train_logging(self, amnl_train_loss, step):
        amnl_train_loss = {str(i): loss for i, loss in enumerate(amnl_train_loss)}
        self.add_scalars('AMNL_train_loss', amnl_train_loss, step)

    def fc_train_logging(self, fc_train_loss, step):
        self.add_scalar('fc_train_loss', fc_train_loss, step)

    def test_logging(self, test_loss, step):
        self.add_scalar('test_loss', test_loss, step)

    def validation_logging(self, validation_loss, step):
        validation_loss = {str(i): loss for i, loss in enumerate(validation_loss)}
        self.add_scalars('validation_loss', validation_loss, step)
